# telegram-bot

## Getting Started

- Install python dan virtual environment
- Tutorial pembuatan bot https://www.petanikode.com/bot-telegram-tanpa-coding/ 
- Tutorial telegram api https://core.telegram.org/bots/api

Proses yang di jalankan :
- Chat pada telegram-bot yang di buat
- Auto replay ketika send
- History log

python yang di gunakan :
```bash
Python 3.7.0
```

jalankan perintah :
```bash
pip install requests==2.21.0
```

Tampilan

![command](screen/command.png)
![telegram](screen/telegram.png)
